const express = require('express')
var mysql = require('mysql')
var app = express();
var path = require('path')
var cors = require('cors');
app.use(express.json())
app.use(cors());

/**
 * 
 * Endpoints
 * 
 */

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/index.html'))
})

app.post('/user/create', (req, res) => {
    console.log("/user/create, about to insert")
    let sqlQuery = "INSERT INTO users (name, last_name, city, date_of_birth, image_url, description, email, password) VALUES " +
    "('" + req.body.name + "', '" + req.body.last_name + "', '" + req.body.city  + "', '" + req.body.date_of_birth  + "', '" + req.body.image_url 
    + "', '" + req.body.description  + "', '" + req.body.email  + "', '" + req.body.password  + "');";
    console.log(sqlQuery)
    performQuery(sqlQuery)
    res.send("OK")
})

app.post('/user/update', (req,res) => {
    console.log("/user/update, about to update")
    let sqlQuery = "UPDATE users SET name = '" + req.body.name + "', last_name = '" + req.body.last_name
    + "', city = '" + req.body.city + "', image_url= '" + req.body.image_url + "', description = '" + req.body.description + "', email = '" 
    + req.body.email + "', password = '" + req.body.password 
    + "' WHERE user_id= '" + req.body.user_id + "';";
    console.log(sqlQuery)
    performQuery(sqlQuery)
    res.send("OK")
})

//?user=name&password=pass
//Obviously we want to do a post, but that requires sessions which is not prioritized for us
app.get('/user/login', (req, res) => {
    console.log("/user/login, about to select")
    let sqlQuery = "SELECT user_id FROM users WHERE name = '" + req.query.user 
    + "' AND password= '" + req.query.password + "';";
    console.log(sqlQuery);
    performQuery(sqlQuery)
        .then((result) => {
            if (result.length > 0) {
                console.log("Length > 0")
                console.log(result[0]["user_id"])
                res.status(200).send( {
                    user_id:result[0]["user_id"] 
                    })
            } else {
                console.log("Length <= 0")
                res.status(403)
                res.end();
            }
        });
});



app.post('/user/like', (req, res) => {
    console.log("In /user/like")
    let sqlQuery = "INSERT INTO likes (user1, user2) VALUES ('" + req.body.user1 + "', '" + req.body.user2 + "');";

    console.log(sqlQuery)
    performQuery(sqlQuery)
    checkMatch(req.body.user1, req.body.user2)
    .then((result) => {
        console.log(result)
        res.send(result)
    })
    .catch(function(err) {
        console.log(err)
        res.send("false")
    })
})

app.get('/user/:id', (req, res) => {
    let sqlQuery = "SELECT name, last_name, user_id, city, date_of_birth, description, image_url, member_since FROM users WHERE user_id =" + req.params.id;
    performQuery(sqlQuery)
        .then(result => {
            if (result.length > 0) {
                res.status(200).send(result);
            } else {
                res.status(200).send([]);
            }
        })
});

app.get('/user/:id/matches/', (req,res) => {
    console.log("In /user/{id}/matches/")
    //req.params.id
    let sqlQuery = "SELECT DISTINCT name, last_name, user_id, city, date_of_birth, image_url FROM likes, users WHERE likes.user2 = users.user_id AND likes.user1 = " 
    + req.params.id  + " AND user_id IN (SELECT users.user_id FROM likes, users WHERE likes.user1 = users.user_id AND likes.user2 =" +req.params.id +")";

    console.log(sqlQuery)
    performQuery(sqlQuery)
    .then((result) => {
        console.log(result)
        res.send(result)
    })

})
app.get('/user/:id/feed', (req, res) => {
    console.log("/user/:id/feed, about to select");
    let id = req.params.id;
    let sqlQuery = "SELECT user_id, name, last_name, city, date_of_birth, image_url, description, member_since " +
        "FROM users " +
        "WHERE user_id != " + id +
        " AND user_id NOT IN (SELECT users.user_id  FROM likes, users WHERE likes.user1 = " + id + " AND likes.user2 = users.user_id)";
    console.log(sqlQuery);
    performQuery(sqlQuery)
        .then(result => {
            if (result.length > 0) {
                res.status(200).send(result)                
            } 
            else {
                res.status(200).send([]);
            }
        })
});

/**
 * 
 * Database functions
 * 
 */

function checkMatch(user1, user2) {

    return new Promise((resolve, reject) => {
        let sqlQuery1 ="SELECT COUNT(*) FROM likes WHERE user1= '" + user1 + "' AND user2='" + user2 + "';";
        console.log(sqlQuery1)
        performQuery(sqlQuery1)
        .then((result1) => {
            console.log(result1[0]["COUNT(*)"])
            if (result1[0]["COUNT(*)"] > 0){
                let sqlQuery2 ="SELECT COUNT(*) FROM likes WHERE user1= '" + user2 + "' AND user2='" + user1 + "';";
                performQuery(sqlQuery2)
                .then((result2) => {
                    if (result2[0]["COUNT(*)"] > 0) {
                        resolve(true)
                    }else {
                        resolve(false)
                    }
                })
            } else{
                resolve(false)
            }
        })
        .catch(function(err) {
            console.log(err)
            reject(err)
        })
    })
}

function performQuery(sqlQuery) {
    let conn = connectToDb();
    return new Promise((result) => {
        conn.query(sqlQuery, (err, res) => {
            if (err) throw "Error in performQuery -> conn.query(): " +  err;
            result(res)
        })
        conn.end();
        console.log("Connection closed.")
        return result;
    })
}

 function connectToDb() {
    let conn = mysql.createConnection(
        {
            host: "localhost",
            port: "3306",
            user: "root",
            password: "",
            database: "hinder_db"
        }
    )
    console.log("Connected..")
    return conn;
 }

app.listen(process.env.PORT || 8080)